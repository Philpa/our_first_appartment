﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour
{
    //0: Negative Z   1: Positive Z
    public bool direction;
    public float speed = 10f;

    // Update is called once per frame
    void Update()
    {
        Vector3 movement = new Vector3(0, 0, direction ? speed : -speed);
        transform.position += movement * Time.deltaTime;
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Destroyed");
        if (other.CompareTag("CarTrigger"))
        {
            Destroy(gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarsSpawn : MonoBehaviour
{
    public GameObject trigger;
    public GameObject carPrefab;

    public bool direction;
    public float minSpawnTime = 2f;
    public float maxSpawnTime = 10f;

    float nextSpawnTimer;

    // Update is called once per frame
    void Update()
    {
        nextSpawnTimer -= Time.deltaTime;
        if(nextSpawnTimer <= 0)
        {
            GameObject newCar = Instantiate(carPrefab, this.transform);
            newCar.GetComponent<CarMovement>().direction = direction;

            nextSpawnTimer = Random.Range(minSpawnTime, maxSpawnTime);
        }
    }
}

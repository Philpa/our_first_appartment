﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //WIP: Letter throw

    public float walkSpeed = 200f;
    public float sprintMultiplier = 2f;
    public float jumpSpeed = 200f;

    public float throwForce = 200f;

    public GameObject letterPrefab;

    Rigidbody rb;
    CapsuleCollider col;
    CameraController camControl;

    Vector3 moveDirection;
    bool isJumping;
    bool isThrowing;

    void Awake()
    {
        //Setup dependencies between scripts
        camControl = GetComponent<CameraController>();
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
    }

    void Update()
    {
        float moveHorizontal = 0f;
        float moveVertical = 0f;

        //Get arrows inputs
        if (CanMove(transform.right * Input.GetAxis("Horizontal")))
        {
            moveHorizontal = Input.GetAxis("Horizontal");
        }
        if(CanMove(transform.forward * Input.GetAxis("Vertical")))
        {
            moveVertical = Input.GetAxis("Vertical");
        }
        
        moveDirection = (moveHorizontal * transform.right + moveVertical * transform.forward).normalized;

        //If the player is jumping, apply the jump.
        if (Input.GetKey(KeyCode.Space) && rb.velocity.y == 0)
        {
            isJumping = true;
        }
        
        if (Input.GetMouseButtonDown(0))
        {
            isThrowing = true;
        }
    }

    void FixedUpdate()
    {
        //Keep the old velocity and move the player
        Vector3 oldVelocity = new Vector3(0, rb.velocity.y, 0);
        rb.velocity = moveDirection * walkSpeed * (Input.GetKey(KeyCode.LeftShift) ? sprintMultiplier : 1) * Time.deltaTime;
        if(isJumping)
        {
            rb.velocity = transform.up * jumpSpeed * Time.deltaTime;
            isJumping = false;
        }
        if(isThrowing)
        {
            GameObject newLetter = Instantiate(letterPrefab, this.transform.position, this.transform.rotation);
            Rigidbody newLetterRb = newLetter.GetComponent<Rigidbody>();
            newLetterRb.velocity = (transform.forward * throwForce).normalized * Time.deltaTime;
            isThrowing = false;
        }
        rb.velocity += oldVelocity;
    }

    bool CanMove(Vector3 direction)
    {
        float distanceToPoints = col.height / 2 - col.radius;

        Vector3 point1 = transform.position + col.center + Vector3.up * distanceToPoints;
        Vector3 point2 = transform.position + col.center - Vector3.up * distanceToPoints;

        float radius = col.radius * 0.95f; //Making the cast just a little smaller
        float castDistance = 0.5f;

        RaycastHit[] hits = Physics.CapsuleCastAll(point1, point2, radius, direction, castDistance);

        foreach (RaycastHit objectHit in hits)
        {
            if (objectHit.transform.tag == "Wall")
            {
                return false;
            }
        }

        return true;
    }
}

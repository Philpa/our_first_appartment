﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public GameObject player;

    private float speedHorizontal;
    private float speedVertical;

    // Start is called before the first frame update
    void Start()
    {
        speedHorizontal = 1f ;
        speedVertical = 1f;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //Follow player
        transform.position = player.transform.position;
        //Move with mouse
        float h = speedHorizontal * Input.GetAxis("Mouse X");
        float v = -(speedVertical * Input.GetAxis("Mouse Y"));
        transform.Rotate(v, h, 0);
    }
}
